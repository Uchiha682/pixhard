﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Controler : MonoBehaviour {

	public GameObject obj;

	public void ativaBotao(){
		obj.SetActive (true);
	}

	public void LoadScene(string scenename)
	{
		SceneManager.LoadScene (scenename);
	}

	public void SairGame()
	{
		Debug.Log("QUIT!");
		Application.Quit();
	}

}