﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class vbInteraction : MonoBehaviour, IVirtualButtonEventHandler
{
    VirtualButtonBehaviour[] virtualButtonBehaviours;
    string vbName, vbClick2;
	public GameObject firstPanel, secondPanel, MRAM, RAMPanel, moverRAM, PanelPlaca, PlacaMae, painelFonte, fonteAlimemtacao, painelCulers, culers, painelHD, hd;
	public Animator RamAnimat;
	string btnName;

    void Start()
    {

        //Register with the virtual buttons TrackableBehaviour
        virtualButtonBehaviours = GetComponentsInChildren<VirtualButtonBehaviour>();

		//moverRAM = GameObject.Find ("VirtualButton2");
		moverRAM.GetComponent<VirtualButtonBehaviour> ().RegisterEventHandler (this);
		RamAnimat.GetComponent<Animator> ();

        for (int i = 0; i < virtualButtonBehaviours.Length; i++)
            virtualButtonBehaviours[i].RegisterEventHandler(this);
    }


    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
		vbName = vb.VirtualButtonName;

		if (vbName == "VirtualButtonChange")
			VirtualButtonChange();
		else if (firstPanel.activeInHierarchy)
		{
			Deactivate();
			if (vbName == "VirtualButton1")
				Btn1();
			else if (vbName == "VirtualButton2")
				Btn2();
		}

		else
		{
			Deactivate();
			if (vbName == "VirtualButton1")
				Btn3();
			else if (vbName == "VirtualButton2")
				Btn4();
		}
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
		RamAnimat.Play ("MRAM");
		Debug.Log ("BTN Realease");
    }

    void VirtualButtonChange()
    {
		if (firstPanel.activeInHierarchy) {
			firstPanel.SetActive (false);
			secondPanel.SetActive (true);
		} else{
			firstPanel.SetActive (true);
			secondPanel.SetActive (false);
		}
    }
		
    void Deactivate()
    {
		RAMPanel.SetActive (false);
        MRAM.SetActive(false);
		PanelPlaca.SetActive(false);
		PlacaMae.SetActive(false);
		painelFonte.SetActive (false);
		fonteAlimemtacao.SetActive (false);
		painelHD.SetActive (false);
		hd.SetActive (false);
		painelCulers.SetActive (false);
		culers.SetActive (false);
    }

    void Btn1()
	{			
		MRAM.SetActive(true);
		RAMPanel.SetActive (true);	
    }

    void Btn2()
	{		
		painelHD.SetActive (true);
		hd.SetActive (true);
		//RamAnimat.Play ("encaixarRAM");
		//Debug.Log ("BTN Pressed");
    }

    void Btn3()
	{			
		PanelPlaca.SetActive (true);
		PlacaMae.SetActive (true);	

    }

    void Btn4()
    {
		painelFonte.SetActive (true);
		fonteAlimemtacao.SetActive (true);
		//RamAnimat.Play ("encaixarRAM");
		//Debug.Log ("BTN Pressed");
    }

	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;

			if (Physics.Raycast (ray, out hit)) {
				if (hit.collider.tag == "btn1FirstPanel") {
					Deactivate();
					Btn1 ();
				}

				if (hit.collider.tag == "btn2FirstPanel") {
					Deactivate();
					Btn2 ();
					}

				if (hit.collider.tag == "btnChangePanel") {
					VirtualButtonChange ();
				}

				if (hit.collider.tag == "btn1SecondPanel") {
					Deactivate();
					Btn3 ();
				}
		
				if (hit.collider.tag == "btn2SecondPanel") {
					Deactivate();
					Btn4 ();
				}
		
			}
  		}
	}

}